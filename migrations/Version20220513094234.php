<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220513094234 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE stop_stop (stop_source INT NOT NULL, stop_target INT NOT NULL, INDEX IDX_53B0D4A9E7BA3897 (stop_source), INDEX IDX_53B0D4A9FE5F6818 (stop_target), PRIMARY KEY(stop_source, stop_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stop_stop ADD CONSTRAINT FK_53B0D4A9E7BA3897 FOREIGN KEY (stop_source) REFERENCES stop (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stop_stop ADD CONSTRAINT FK_53B0D4A9FE5F6818 FOREIGN KEY (stop_target) REFERENCES stop (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stop DROP FOREIGN KEY FK_B95616B615ED8D43');
        $this->addSql('DROP INDEX IDX_B95616B615ED8D43 ON stop');
        $this->addSql('ALTER TABLE stop DROP tour_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE stop_stop');
        $this->addSql('ALTER TABLE stop ADD tour_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stop ADD CONSTRAINT FK_B95616B615ED8D43 FOREIGN KEY (tour_id) REFERENCES tour (id)');
        $this->addSql('CREATE INDEX IDX_B95616B615ED8D43 ON stop (tour_id)');
    }
}

<?php

namespace App\Entity;

use App\Repository\StopRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StopRepository::class)
 */
class Stop
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToMany(targetEntity=Stop::class, inversedBy="stops")
     */
    private $tour;

    /**
     * @ORM\ManyToMany(targetEntity=Stop::class, mappedBy="tour")
     */
    private $stops;

    public function __construct()
    {
        $this->tour = new ArrayCollection();
        $this->stops = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getTour(): Collection
    {
        return $this->tour;
    }

    public function addTour(self $tour): self
    {
        if (!$this->tour->contains($tour)) {
            $this->tour[] = $tour;
        }

        return $this;
    }

    public function removeTour(self $tour): self
    {
        $this->tour->removeElement($tour);

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getStops(): Collection
    {
        return $this->stops;
    }

    public function addStop(self $stop): self
    {
        if (!$this->stops->contains($stop)) {
            $this->stops[] = $stop;
            $stop->addTour($this);
        }

        return $this;
    }

    public function removeStop(self $stop): self
    {
        if ($this->stops->removeElement($stop)) {
            $stop->removeTour($this);
        }

        return $this;
    }

    
    public function getAllCompany(?Tour $tour): self{
        
        return $tour->getCompany();
    }
}

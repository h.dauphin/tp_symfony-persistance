<?php

namespace App\Controller;

use App\Entity\Tour;
use App\Form\TourType;
use App\Repository\TourRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class TourController extends AbstractController
{
    /**
     * @Route("/tour/", name="tour_index", methods={"GET"})
     */
    public function index(TourRepository $tourRepository): Response
    {
        return $this->render('tour/index.html.twig', [
            'tours' => $tourRepository->findAll(),
        ]);
    }

    /**
     * @Route("/tour/new", name="tour_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TourRepository $tourRepository): Response
    {
        $tour = new Tour();
        $form = $this->createForm(TourType::class, $tour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tourRepository->add($tour, true);

            return $this->redirectToRoute('tour_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tour/new.html.twig', [
            'tour' => $tour,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tour_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Tour $tour, TourRepository $tourRepository): Response
    {
        $form = $this->createForm(TourType::class, $tour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tourRepository->add($tour, true);

            return $this->redirectToRoute('tour_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tour/edit.html.twig', [
            'tour' => $tour,
            'form' => $form,
        ]);
    }
}
